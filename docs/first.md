# C Files

---

## File Handling

In C, you can create, open, read, and write to files by declaring a pointer of type <span style="color: red;">FILE</span>, and use the <span style="color: red;">fopen()</span> function:

<div style="background-color: #2d333b; padding: 10px; border-radius: 10px; border: 2px solid #3e4451;">
    <pre style="color: #e5e5e5;">
<code>
FILE *fptr;

fptr = fopen(filename, mode);
</code>
    </pre>
</div><br>

<span style="color: red;">FILE</span> is basically a data type, and we need to create a pointer variable to work with it (<span style="color: red;">fptr</span>). For now, this line is not important. It's just something you need when working with files.

To actually open a file, use the <span style="color: red;">fopen()</span> function, which takes two parameters:

| __Parameter__ | __Description__                                                                                                                                                                        |
| ------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| *filename*    | The name of the actual file you want to open (or create), like <span style="color: red;">filename.txt</span>                                                                                                            |
| *mode*        | A single character, which represents what you want to do with the file (read, write or append):<br><br><span style="color: red;">w</span> - Writes to a file<br><span style="color: red;">a</span> - Appends new data to a file<br><span style="color: red;">r</span> - Reads from a file |
<br>

---

## Create a File

To create a file, you can use the <span style="color: red;">w</span> mode inside the <span style="color: red;">fopen()</span> function.

The <span style="color: red;">w</span> mode is used to write to a file. **However**, if the file does not exist, it will create one for you:<br>

<div style="background-color: #2d333b; padding: 10px; border-radius: 10px; border: 2px solid #3e4451;"><h3>Example</h3>
    <pre style="color: #e5e5e5;">
<code>
FILE *fptr;

// Create a file
fptr = fopen("filename.txt", "w");

// Close the file
fclose(fptr);
</code>
    </pre>
<strong>Note :</strong> The file is created in the same directory as your other C files, if nothing else is specified.<br>
On our computer, it looks like this:<br>

<img src="/c_files_create.png">
</div><br>

**Tip**: If you want to create the file in a specific folder, just provide an absolute path: <br>

<div style="background-color: #2d333b; padding: 10px; border-radius: 10px; border: 2px solid #3e4451;">
    <pre style="color: #e5e5e5;">
<code>
fptr = fopen("C:\directoryname\filename.txt", "w");
</code>
    </pre>
</div><br>

<div style="background-color: #ffeb99; color: black; padding: 10px; border: 2px solid #3e4451;">
<h3>Closing the file</h3>

Did you notice the <span style="color: red;">fclose()</span> function in our example above?<br>

This will close the file when we are done with it.<br>

It is considered as good practice, because it makes sure that: <br>

<ul>
    <li>Changes are saved properly</li>
    <li>Other programs can use the file (if you want)</li>
    <li>Clean up unnecessary memory space</li>
</ul>
</div><br>

In the next chapters, you will learn how to write content to a file and read from it.<br>

---
