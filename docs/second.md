# C Write To Files

---

## Write To a File

Let's use the ``w`` mode from the previous chapter again, and write something to the file we just created.<br>

The `w` mode means that the file is opened for **writing**. To insert content to it, you can use the ``fprintf()`` function and add the pointer variable (``fptr`` in our example) and some text:<br>

> <h3>Example</h3>
>
> ```c
> FILE *fptr;
>
> // Open a file in writing mode
> fptr = fopen("filename.txt", "w");
>
> // Write some text to the file
> fprintf(fptr, "Some text");
>
> // Close the file
> fclose(fptr);
> ```
>
> As a result, when we open the file on our computer, it looks like this: <br>
> <img src="/c_write.png">
> <br>

<div style="background-color: #ffeb99; color: black; padding: 10px; border: 2px solid #3e4451;">
<strong>Note:</strong> If you write to a file that already exists, the old content is deleted, and the new content is inserted. This is important to know, as you might accidentally erase existing content.<br>
<br>
For example: <br>
<br>
<div style="background-color: #38444D; color: black; padding: 10px; border: 2px solid #3e4451;">
<h3>Example</h3>

<pre>
<code>
fprintf(fptr, "Hello World!");
</code>
</pre>
As a result, when we open the file on our computer, it says "Hello World!" instead of "Some text":<br>
<img src="/c_write2.png">
</div><br>

</div>

---

## Append Content To a File

If you want to add content to a file without deleting the old content, you can use the ``a`` mode.<br>

The ``a`` mode appends content at the end of the file:<br>

> <h3>Example</h3>
>
> ```c
> FILE *fptr;
>
> // Open a file in append mode
> fptr = fopen("filename.txt", "a");
>
> // Append some text to the file
> fprintf(fptr, "\nHi everybody!");
>
> // Close the file
> fclose(fptr);
> ```
>
> As a result, when we open the file on our computer, it looks like this:<br>
> <img src="/c_files_append.png">
><br>

<div style="background-color: #ffeb99; color: black; padding: 10px; border: 2px solid #3e4451;">
<strong>Note:</strong> Just like with the w mode; if the file does not exist, the a mode will create a new file with the "appended" content.<br>
</div>
