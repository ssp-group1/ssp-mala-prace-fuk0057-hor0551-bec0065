# Naše malá práce

---
## O co se jedná

Toto je "Malá práce" pro předmět **Správa softwarových projektů (SSP)** <br>
<br>
Jako ukázku jsme si vybrali vypracovat napodobeninu [tohoto](https://www.w3schools.com/c/c_files.php) tutoriálu k práci se soubory v jazyce C <br>
Každý z nás si vybral zpracovat jednu ze tří částí tohoto tutoriálu.<br>

---

## Vypracovali

Studenti 2. ročníku<br>
VŠB – Technická univerzita Ostrava, Fakulta elektrotechniky a informatiky, obor Informatika<br>
**Fukalová Eva** (FUK0057)<br>
**Horák Lukáš** (HOR0551)<br>
**Běčáková Simona** (BEC0065)<br>

**Vypracováno:** jaro 2024
