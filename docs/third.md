# C Read Files

---

## Read a File

In the previous chapter, we wrote to a file using <font style="background-color: gray; color: orange;"> __w__ </font> and <font style="background-color: gray; color: orange;"> __a__ </font> modes inside the fopen() function.

To __read__ from a file, you can use <font style="background-color: gray; color: orange;"> __r__ </font> mode:

> <font style="font-size: 22px;">__Example__</font>
>
> ```c
> FILE *fptr;
>
> // Open a file in read mode
> fptr = fopen("filename.txt", "r");
> ```

This will make <font style="background-color: gray; color: orange;"> __filename.txt__ </font> opened for reading.

<div style="background-color: yellow; color: black;">It requires a little bit of work to read a file in C. Hang in there! We will guide you step-by-step.</div>

Next, we need to create a string that should be big enough to store the content of the file.

For example, let's create a string that can store up to 100 characters:

> <font style="font-size: 22px;">__Example__</font>
> ```c
> FILE *fptr;
>
> // Open a file in read mode
> fptr = fopen("filename.txt", "r");
>
> // Store the content of the file
> char myString[100];
> ```

In order to read the content of <font style="background-color: gray; color: orange;"> __filename.txt__ </font>, we can use the fgets() function.

The fgets() function takes three parameters:

> <font style="font-size: 22px;">__Example__</font>
> ```c
> fgets(myString, 100, fptr);
> ```

1. The first parameter specifies where to store the file content, which will be in the myString array we just created.
1. The second parameter specifies the maximum size of data to read, which should match the size of myString (100).
1. The third parameter requires a file pointer that is used to read the file (fptr in our example).

Now, we can print the string, which will output the content of the file:

> <font style="font-size: 22px;">__Example__</font>
>
> ```c
> FILE *fptr;
>
> // Open a file in read mode
> fptr = fopen("filename.txt", "r");
>
> // Store the content of the file
> char myString[100];
>
> // Read the content and store it inside myString
> fgets(myString, 100, fptr);
>
> // Print the file content
> printf("%s", myString);
>
> // Close the file
> fclose(fptr);
> ```
>
> ```text
> Hello World!
> ```
> <a href="https://www.w3schools.com/cpp/showcpp.asp?filename=c_demo_files_read"><button>Run example</button></a>

__Note:__ The <font style="background-color: gray; color: orange;"> __fgets__ </font> function only reads the first line of the file. If you remember, there were two lines of text in <font style="background-color: gray; color: orange;"> __filename.txt__ </font>.

To read every line of the file, you can use a <font style="background-color: gray; color: orange;"> <u>__while__</u> </font> loop:

> <font style="font-size: 22px;">__Example__</font>
>
> ```c
> FILE *fptr;
>
> // Open a file in read mode
> fptr = fopen("filename.txt", "r");
>
> // Store the content of the file
> char myString[100];
>
> // Read the content and print it
> while(fgets(myString, 100, fptr)) {
>     printf("%s", myString);
> }
>
> // Close the file
> fclose(fptr);
> ```
>
> ```text
> Hello World!
> Hi everybody!
> ```
>
> <a href="https://www.w3schools.com/cpp/showcpp.asp?filename=c_demo_files_read_while"><button>Run example</button></a>

---

## __Good Practice__

If you try to open a file for reading that does not exist, the <font style="background-color: gray; color: orange;"> __fopen()__ </font> function will return <font style="background-color: gray; color: orange;"> __NULL__ </font>.

__Tip:__ As a good practice, we can use an <font style="background-color: gray; color: orange;"><u>__if__</u></font> statement to test for <font style="background-color: gray; color: orange;">__NULL__</font>, and print some text instead (when the file does not exist):

> <font style="font-size: 22px;">__Example__</font>
>
> ```c
> FILE *fptr;
>
> // Open a file in read mode
> fptr = fopen("loremipsum.txt", "r");
>
> // Print some text if the file does not exist
> if(fptr == NULL) {
>     printf("Not able to open the file.");
> }
>
> // Close the file
> fclose(fptr);
> ```
> If the file does not exist, the following text is printed:
> ```text
> Not able to open the file.
> ```
> <a href="https://www.w3schools.com/cpp/showcpp.asp?filename=c_demo_files_null"><button>Run example</button></a>

With this in mind, we can create a more sustainable code if we use our "read a file" example above again:

> <font style="font-size: 22px;">__Example__</font>
>
> If the file exists, read the content and print it. If the file does not exist, print a message:
>
> ```c
> FILE *fptr;
>
> // Open a file in read mode
> fptr = fopen("filename.txt", "r");
>
> // Store the content of the file
> char myString[100];
>
> // If the file exists
> if(fptr != NULL) {
>
>     // Read the content and print it
>     while(fgets(myString, 100, fptr)) {
>         printf("%s", myString);
>     }
>
> // If the file does not exist
> } else {
>     printf("Not able to open the file.");
> }
>
> // Close the file
> fclose(fptr);
> ```
>
> ```text
> Hello World!
> Hi everybody!
> ```
>
> <a href="https://www.w3schools.com/cpp/showcpp.asp?filename=c_demo_files_null2"><button>Run example</button></a>

---

<button>< Previous</button> <button style="text-align: right; padding-right:5px">Next ></button>
