# Malý projekt do předmětu SSP

Toto je "Malá práce" pro předmět **Správa softwarových projektů (SSP)** <br>
<br>
Jako ukázku práce jsme si vybrali vypracovat napodobeninu [tohoto](https://www.w3schools.com/c/c_files.php) tutoriálu k práci se soubory v jazyce C <br>
Každý z nás si vybral zpracovat jednu ze tří částí tohoto tutoriálu. Proto každá z těchto stránek vypadá lehce jinak. Chtěli jsme ukázat samostatně co umíme ale v rámci jednoho projektu.<br>
Naši práci můžete vidět [zde](https://ssp-mala-prace-fuk0057-hor0551-bec0065-ssp-group-2eb38ba5fe283f.gitlab.io/)

## Zadání

"Malá" práce
Připravte repozitář na Gitlabu, který bude používat generátor statických stránek ve spojení s kontinuální integrací (CI). CI bude generovat HTML stránky z Markdown podkladu. Součástí CI by měl být také test kvality Markdown souborů. Práce vychází z cvičení č. 6, kde jsou uvedeny i případné vhodné kontejnery.
Požadavky na kvalitu práce (hodnotící kritéria):

- [X] GIT repozitář na Gitlabu.
- [X] Repozitář má README.md (K čemu projekt slouží, jak se používá, jak se instaluje, jaké jsou prerekvizity, apod.)
- [X] Použití libovolného generátoru statických stránek dle vlastního výběru. (MKdocs, Middleman, Jekyll, apod.)
- [X] Vytvořená CI v repozitáři.
- [X] CI má minimálně dvě úlohy:
- [X] Test kvality Markdown stránek.
- [X] Generování HTML stránek z Markdown zdrojů.
- [X] CI má automatickou úlohou nasazení web stránek (deploy).
- [X] Gitlab projekt má fukční web stránky s generovaným obsahem na URL

## Vypracovali

Studenti 2. ročníku<br>
VŠB – Technická univerzita Ostrava, Fakulta elektrotechniky a informatiky<br>
Obor Informatika <br>
**Vypracováno:** jaro 2024

### Fukalová Eva (FUK0057)

### Horák Lukáš (HOR0551)

### Běčáková Simona (BEC0065)
